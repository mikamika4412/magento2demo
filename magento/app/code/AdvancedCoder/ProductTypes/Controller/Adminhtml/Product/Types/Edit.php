<?php

namespace AdvancedCoder\ProductTypes\Controller\Adminhtml\Product\Types;

use AdvancedCoder\ProductTypes\Model\ProductTypesRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use AdvancedCoder\ProductTypes\Api\Data\ProductTypesInterface;

class Edit extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::product_types';
    private ProductTypesRepository $productTypesRepository;

    public function __construct(
        Context $context,
        ProductTypesRepository $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }


    public function execute(): ResultInterface
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $type = $this->productTypesRepository->get($id);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('AdvancedCoder_ProductTypes::advanced_coder')
                ->addBreadcrumb(__('Edit Product Type'), __('Product Type'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit Product Type: %type', ['type' => $type->getType()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Product type with id "%value" does not exist.', ['value' => $id])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}
