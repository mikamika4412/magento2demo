<?php

namespace Mika\HelloWorld\Model\ResourceModel\Item;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Mika\HelloWorld\Model\Item;
use Mika\HelloWorld\Model\ResourceModel\Item as ItemResource;

/**
 * class Collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
 */
class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'news_id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Item::class, ItemResource::class);
    }
}

