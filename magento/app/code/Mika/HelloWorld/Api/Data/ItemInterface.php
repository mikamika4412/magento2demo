<?php

namespace Mika\HelloWorld\Api\Data;

//use Magento\Tests\NamingConvention\true\string;
//use Magento\Tests\NamingConvention\true\string;

/**
 ** Interface ItemInterface
 * Get news and comments
 *
 * @api
 */
interface ItemInterface
{
    public const ID = 'news_id';
    public const TYPE = 'news';

    /**
     * @return string
     */
    public function getNews():string;

    /**
     * @param string $news
     *
     * @return void
     */
    public function setNews(string $news):void;

    /**
     * @return string|null
     */
    public function getComment();

    /**
     * @return mixed
     */
    public function getId();
    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);
}
