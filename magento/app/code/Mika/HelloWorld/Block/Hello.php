<?php

namespace Mika\HelloWorld\Block;

use Magento\Framework\View\Element\Template;
use Mika\HelloWorld\Model\ResourceModel\Item\Collection;
use Mika\HelloWorld\Model\ResourceModel\Item\CollectionFactory;

/**
 * Hello class
 * @hello
 */
class Hello extends Template
{
    /**
     * Collection factory
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param Template\Context $context
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * getItems
     * array $data = [] Collection
     */
    public function getItems(): array
    {
        return $this->collectionFactory->create()->getItems();
    }
}
